terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = ">= 3.8"
    }
  }

  backend "azurerm" {
    resource_group_name  = "Cohort29_DipMak_ProjectExercise"
    storage_account_name = "tododmstorageaccount"
    container_name       = "todo-dm-container"
    key                  = "terraform.tfstate"
  }

}

provider "azurerm" {
  features {}
}

data "azurerm_resource_group" "main" {
  name = "Cohort29_DipMak_ProjectExercise"
}

resource "azurerm_service_plan" "main" {
  name                = "${var.prefix}-terraformed-asp"
  location            = data.azurerm_resource_group.main.location
  resource_group_name = data.azurerm_resource_group.main.name
  os_type             = "Linux"
  sku_name            = "B1"
}

resource "azurerm_linux_web_app" "main" {
  name                = "${var.prefix}-terraformed-wa"
  location            = data.azurerm_resource_group.main.location
  resource_group_name = data.azurerm_resource_group.main.name
  service_plan_id     = azurerm_service_plan.main.id

  site_config {
    application_stack {
      docker_image_name   = "dmakwan312/todo-app:latest"
      docker_registry_url = "https://index.docker.io"
    }
  }

  app_settings = {
    "MONGO_CONNECTION_STRING"           = azurerm_cosmosdb_account.main.primary_mongodb_connection_string
    "FLASK_APP"                         = "todo_app/app"
    "SECRET_KEY"                        = "${var.secret_key}"
    "MONGO_DB_NAME"                     = resource.azurerm_cosmosdb_mongo_database.main.name
    "MONGO_COLLECTION_NAME"             = "item-collection"
    "MONGO_ITEM_MIGRATION_TRACKER_NAME" = "item-migration-tracker"
    "TRELLO_API_KEY"                    = "${var.trello_api_key}"
    "TRELLO_API_TOKEN"                  = "${var.trello_api_token}"
    "TRELLO_BOARD_ID"                   = "${var.trello_board_id}"
  }
}

resource "azurerm_cosmosdb_account" "main" {
  name                = "${var.prefix}-terraformed-cosmos-db-account"
  location            = data.azurerm_resource_group.main.location
  resource_group_name = data.azurerm_resource_group.main.name
  offer_type          = "Standard"
  kind                = "MongoDB"

  capabilities {
    name = "EnableServerless"
  }

  consistency_policy {
    consistency_level = "Session"
  }

  geo_location {
    location          = "uksouth"
    failover_priority = 0
  }

  capabilities { name = "EnableMongo" }

}


resource "azurerm_cosmosdb_mongo_database" "main" {
  name                = "${var.prefix}-terraformed-cosmos-db"
  resource_group_name = data.azurerm_resource_group.main.name
  account_name        = resource.azurerm_cosmosdb_account.main.name
  lifecycle { prevent_destroy = true }
}
