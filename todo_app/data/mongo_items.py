import pymongo
from datetime import datetime, date
import todo_app.mongo_config as mongo_config
from todo_app.data.Item import Item
from bson import ObjectId

def get_collection():
    client = pymongo.MongoClient(mongo_config.get_mongo_connection_string())
    db = client[mongo_config.get_mongo_database_name()]
    return db[mongo_config.get_mongo_collection_name()]

def get_items():
    """
    Fetches all saved items from the Mongo collection.

    Returns:
        list: The list of saved items.
    """
    
    collection = get_collection()
    document_cursor = collection.find({})

    items = []

    for document in document_cursor:
        items.append(Item.from_mongo_document(document))
    
    return items


def mark_complete(id):
    """
    Mark a document as 'DONE'
    """

    object_id = create_object_id(id)

    collection = get_collection()
    collection.find_one_and_update({'_id': object_id}, {'$set': {'status': mongo_config.DONE_STATUS}})

def mark_incomplete(id):
    """
    Mark a document as 'TODO'
    """

    object_id = create_object_id(id)

    collection = get_collection()
    collection.find_one_and_update({'_id': object_id}, {'$set': {'status': mongo_config.TO_DO_STATUS}})

def delete_item(id):
    """
    Deletes a document
    """

    object_id = create_object_id(id)
    
    collection = get_collection()
    collection.delete_one({'_id': object_id})

def add_item(name):
    """
    Creates a document
    """
    collection = get_collection()
    collection.insert_one({'name': name, 'dateLastActivity': datetime.now().strftime("%Y-%m-%dT%H:%M:%S.%fZ"), 'status': mongo_config.TO_DO_STATUS})

def add_item_from_trello(name, dateLastActivity, status):
    collection = get_collection()
    collection.insert_one({'name': name, 'dateLastActivity': dateLastActivity.strftime("%Y-%m-%dT%H:%M:%S.%fZ"), 'status': status})


def create_object_id(id):
    return ObjectId(id)