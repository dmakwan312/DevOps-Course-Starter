# Configuration for Trello API interaction
import os

TO_DO_STATUS = 'To Do'
DONE_STATUS = 'Done'

def get_mongo_connection_string():
    return os.environ.get('MONGO_CONNECTION_STRING')

def get_mongo_database_name():
    return os.environ.get('MONGO_DB_NAME')

def get_mongo_collection_name():
    return os.environ.get('MONGO_COLLECTION_NAME')

def get_mongo_item_migration_tracker_name():
    return os.environ.get('MONGO_ITEM_MIGRATION_TRACKER_NAME')


