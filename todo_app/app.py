from flask import Flask, render_template, redirect, request

from todo_app.flask_config import Config
from todo_app.data.mongo_items import get_items, add_item, mark_complete, mark_incomplete, delete_item
from todo_app.view_model import ViewModel
from todo_app.data_migrater import migrate_data_from_trello

def create_app():
    app = Flask(__name__)
    app.config.from_object(Config())

    @app.route('/')
    def index():
        item_view_model = ViewModel(sorted(get_items(), key=lambda k : k.status, reverse=True))
        return render_template('index.html', view_model=item_view_model)

    @app.route('/addToDo', methods=['POST'])
    def add_to_do():
        add_item(request.form.get('to_do_title'))
        return redirect('/')

    @app.route('/complete_item', methods=['POST'])
    def complete_item():
        item_id = request.form.get('itemId')
        item_status = request.form.get('status')
        mark_complete(item_id) if item_status == 'true' else mark_incomplete(item_id)
        return redirect('/')

    @app.route('/deleteItem', methods=['POST'])
    def deleteItem():
        item_id = request.form.get('itemId')
        delete_item(item_id)
        return redirect('/')

    @app.route('/migrateData', methods=['POST'])
    def migrateData():
        migrate_data_from_trello()
        return redirect('/')
        
    return app