from todo_app.data.trello_items import get_items
from todo_app.data.mongo_items import add_item_from_trello
import todo_app.mongo_config as mongo_config
import pymongo

def migrate_data_from_trello():
    items_to_migrate = find_items_not_migrated()
    for item in items_to_migrate:
        add_item_from_trello(item.name, item.last_modified, item.status)
    mark_migrated(items_to_migrate)

def mark_migrated(items):
    collection = get_collection()
    for item in items:
        collection.insert_one({'trello_item_id': item.id})

def find_items_not_migrated():
    trello_items = get_items()
    migrated_mongo_items = get_migrated_ids()

    not_in_mongo = [item for item in trello_items if item.id not in migrated_mongo_items]
    return not_in_mongo

def get_migrated_ids():
    collection = get_collection()
    document_cursor = collection.find({})

    items = []

    for document in document_cursor:
        items.append(document['trello_item_id'])
    return items

def get_collection():
    client = pymongo.MongoClient(mongo_config.get_mongo_connection_string())
    db = client[mongo_config.get_mongo_database_name()]
    return db[mongo_config.get_mongo_item_migration_tracker_name()]