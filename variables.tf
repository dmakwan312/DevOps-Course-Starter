variable "prefix" {
  description = "The prefix used for all resources in this environment"
}

variable "secret_key" {
  description = "Secret key required for flask"
  sensitive = "true"
}

variable "mongo_collection_name" {
  description = "Name of Mongo collection used to track items"
  default = "item-collection"
}

variable "mongo_item_migration_tracker_name" {
  description = "Name of Mongo collection used to track item migration from Trello to Mongo"
  default = "item-migration-tracker"
}

variable "trello_api_key" {
  description = "Trello API key"
  sensitive = "true"
}

variable "trello_api_token" {
  description = "Trello API token"
  sensitive = "true"
}

variable "trello_board_id" {
  description = "Trello board ID"
  sensitive = "true"
}