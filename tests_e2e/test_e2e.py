import os
import pytest
from threading import Thread
from time import sleep
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.chrome.options import Options
from dotenv import load_dotenv
from todo_app import app
from todo_app.data.mongo_items import get_items
import pymongo
import todo_app.mongo_config as mongo_config

@pytest.fixture(scope='module')
def app_with_temp_mongo_collection():
    # Load our real environment variables
    load_dotenv(override=True)

    create_test_collection()

    # Construct the new application
    application = app.create_app()

    # Start the app in its own thread.
    thread = Thread(target=lambda: application.run(use_reloader=False))
    thread.daemon = True
    thread.start()
    
    # Give the app a moment to start
    sleep(10)

    # Return the application object as the result of the fixture
    yield application

    # Tear down
    thread.join(1)

def create_test_collection():
    client = pymongo.MongoClient(mongo_config.get_mongo_connection_string())
    database = client[os.environ.get('MONGO_DB_NAME')]
    return database[os.environ.get('MONGO_COLLECTION_NAME')]

@pytest.fixture(scope="module")
def driver():
    chrome_options = Options()
    chrome_options.add_argument('--headless')
    chrome_options.add_argument('--no-sandbox')
    chrome_options.add_argument('--disable-dev-shm-usage')
    with webdriver.Chrome(options=chrome_options) as driver:
        yield driver

@pytest.mark.order1
def test_task_journey_creation(driver, app_with_temp_mongo_collection):
    driver.get('http://localhost:5000/')

    assert driver.title == 'To-Do App'

    WebDriverWait(driver, 30).until(EC.element_to_be_clickable((By.XPATH, '//button[text()="Create"]')))

    input_text_box = driver.find_element("id", "to_do_title")
    input_text_box.send_keys('Task 1')

    create_button = driver.find_element(By.XPATH,'//button[text()="Create"]')
    create_button.click()

    item_id = get_item_id('Task 1');
    if item_id == None:
        pytest.fail('Item not created in MongoDB')
    else:
        WebDriverWait(driver, 30).until(EC.visibility_of_element_located((By.ID, "item_" + item_id)))
        assert check_element_exists_by_id(driver, item_id)
        assert not check_item_status(driver, item_id)

@pytest.mark.order2
def test_task_journey_mark_complete(driver, app_with_temp_mongo_collection):
    driver.get('http://localhost:5000/')

    item_id = get_item_id('Task 1');

    if item_id == None:
        pytest.fail('Item not created in MongoDB')

    WebDriverWait(driver, 30).until(EC.element_to_be_clickable((By.ID, "status_" + item_id)))

    item_checkbox = driver.find_element('id', 'status_' + item_id)
    item_checkbox.click()

    item_id = get_item_id('Task 1');
        
    WebDriverWait(driver, 30).until(EC.visibility_of_element_located((By.ID, "item_" + item_id)))
    assert check_element_exists_by_id(driver, item_id)
    assert check_item_status(driver, item_id)

@pytest.mark.order3
def test_task_journey_mark_incomplete(driver, app_with_temp_mongo_collection):
    driver.get('http://localhost:5000/')

    item_id = get_item_id('Task 1');
    if item_id == None:
        pytest.fail('Item not created in MongoDB')
    
    WebDriverWait(driver, 30).until(EC.element_to_be_clickable((By.ID, "status_" + item_id)))
    item_checkbox = driver.find_element('id', 'status_' + item_id)
    item_checkbox.click()

    item_id = get_item_id('Task 1');
    if item_id == None:
        pytest.fail('Item not created in MongoDB')

    WebDriverWait(driver, 30).until(EC.visibility_of_element_located((By.ID, "item_" + item_id)))
    assert check_element_exists_by_id(driver, item_id)
    assert not check_item_status(driver, item_id)

@pytest.mark.order4
def test_task_journey_mark_delete(driver, app_with_temp_mongo_collection):
    driver.get('http://localhost:5000/')
    
    item_id = get_item_id('Task 1');
    if item_id == None:
        pytest.fail('Item not created in MongoDB')
    
    delete_button = driver.find_element(By.XPATH,'//button[text()="Delete"]')
    delete_button.click()

    WebDriverWait(driver, 30).until(EC.invisibility_of_element((By.ID, "item_" + item_id)))

def check_item_status(driver, id):
    item_checkbox = driver.find_element('id', 'status_' + id)
    return item_checkbox.is_selected()

def get_item_id(name):
    items = get_items()
    for item in items:
        if item.name == name:
            return str(item.id)
    return None

def check_element_exists_by_id(driver, id):
    try:
        driver.find_element('id', 'item_' + id)
    except NoSuchElementException:
        return False
    return True
