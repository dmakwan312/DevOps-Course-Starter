from dotenv import load_dotenv, find_dotenv
from todo_app import app
import todo_app.mongo_config as mongo_config

import pytest
import mongomock
import pymongo


from bson import ObjectId

mocked_items = [{'_id': ObjectId('6669d224111781ff1e9ec07b'), 'name': 'Test card', 'dateLastActivity': '2023-01-01T12:00:00.000Z', 'status': mongo_config.TO_DO_STATUS}, 
                {'_id': ObjectId('6669d23b111781ff1e9ec07c'), 'name': 'Test Complete Mark', 'dateLastActivity': '2023-01-01T12:00:00.000Z', 'status': mongo_config.TO_DO_STATUS},
                {'_id': ObjectId('6669d247111781ff1e9ec07d'), 'name': 'Test Incomplete Mark', 'dateLastActivity': '2023-01-01T12:00:00.000Z', 'status': mongo_config.DONE_STATUS},
                {'_id': ObjectId('6669d251111781ff1e9ec07e'), 'name': 'Test Deletion', 'dateLastActivity': '2023-01-01T12:00:00.000Z', 'status': mongo_config.DONE_STATUS}]

@pytest.fixture
def client():
    # Use our test integration config instead of the 'real' version
    file_path = find_dotenv('.env.test')
    load_dotenv(file_path, override=True)

    # Use the app to create a test_client that can be used in our tests.
    with mongomock.patch(servers=((mongo_config.get_mongo_connection_string(), 27017),)):

        client = pymongo.MongoClient(mongo_config.get_mongo_connection_string(), 27017)
        mongo_database = client[mongo_config.get_mongo_database_name()]
        todo_items = mongo_database[mongo_config.get_mongo_collection_name()]
    
        todo_items.insert_many(mocked_items)

        test_app = app.create_app()

        with test_app.test_client() as client:
            yield client


@pytest.mark.order1
def test_index_page(monkeypatch, client):
    # This replaces any call to requests.get with our own function
    response = client.get('/')

    assert response.status_code == 200
    assert 'Test card' in response.data.decode()
    assert 'To Do Tasks' in response.data.decode()
    assert 'Done Tasks' in response.data.decode()

@pytest.mark.order2
def test_add_item(monkeypatch, client):
    response = client.post('/addToDo', data={'to_do_title': 'Created in Integration Test'})

    assert response.status_code == 302

    response = client.get('/')
    
    assert 'Test card' in response.data.decode()
    assert 'To Do Tasks' in response.data.decode()
    assert 'Done Tasks' in response.data.decode()
    assert 'Created in Integration Test' in response.data.decode()

@pytest.mark.order3
def test_mark_item_complete(monkeypatch, client):
    response = client.post('/complete_item', data={'itemId': ObjectId('6669d23b111781ff1e9ec07c'), 'status': 'true'})

    assert response.status_code == 302

    response = client.get('/')
    
    assert 'Test card' in response.data.decode()
    assert 'To Do Tasks' in response.data.decode()
    assert 'Done Tasks' in response.data.decode()
    assert 'Test Complete Mark' in response.data.decode()
    assert 'id="status_6669d23b111781ff1e9ec07c"  checked' in response.data.decode()

@pytest.mark.order4
def test_mark_item_incomplete(monkeypatch, client):
    response = client.post('/complete_item', data={'itemId': ObjectId('6669d247111781ff1e9ec07d'), 'status': 'false'})

    assert response.status_code == 302
    
    response = client.get('/')
    
    assert 'Test card' in response.data.decode()
    assert 'To Do Tasks' in response.data.decode()
    assert 'Done Tasks' in response.data.decode()
    assert 'Test Incomplete Mark' in response.data.decode()
    assert 'id="status_6669d247111781ff1e9ec07d"  checked' not in response.data.decode()

@pytest.mark.order5
def test_delete_items(monkeypatch, client):
    response = client.post('/deleteItem', data={'itemId': ObjectId('6669d251111781ff1e9ec07e')})

    assert response.status_code == 302

    response = client.get('/')
    
    assert 'Test card' in response.data.decode()
    assert 'To Do Tasks' in response.data.decode()
    assert 'Done Tasks' in response.data.decode()
    assert 'Created in Integration Test' not in response.data.decode()
   