# DevOps Apprenticeship: Project Exercise

> If you are using GitPod for the project exercise (i.e. you cannot use your local machine) then you'll want to launch a VM using the [following link](https://gitpod.io/#https://github.com/CorndelWithSoftwire/DevOps-Course-Starter). Note this VM comes pre-setup with Python & Poetry pre-installed.

## System Requirements

The project uses poetry for Python to create an isolated environment and manage package dependencies. To prepare your system, ensure you have an official distribution of Python version 3.8+ and install Poetry using one of the following commands (as instructed by the [poetry documentation](https://python-poetry.org/docs/#system-requirements)):

### Poetry installation (Bash)

```bash
curl -sSL https://install.python-poetry.org | python3 -
```

### Poetry installation (PowerShell)

```powershell
(Invoke-WebRequest -Uri https://install.python-poetry.org -UseBasicParsing).Content | py -
```

## Dependencies

The project uses a virtual environment to isolate package dependencies. To create the virtual environment and install required packages, run the following from your preferred shell:

```bash
$ poetry install
```

You'll also need to clone a new `.env` file from the `.env.template` to store local configuration options. This is a one-time operation on first setup:

```bash
$ cp .env.template .env  # (first time only)
```

The `.env` file is used by flask to set environment variables when running `flask run`. This enables things like development mode (which also enables features like hot reloading when you make a file change). There's also a [SECRET_KEY](https://flask.palletsprojects.com/en/1.1.x/config/#SECRET_KEY) variable which is used to encrypt the flask session cookie.

## Mongo Setup

In the newly created `.env` file, the variables are set to allow local running through docker compose. This will spin up the application itself, test runners and a dummy MongoDB

## Running the App

Once the all dependencies have been installed, start the Flask app in development mode within the Poetry environment by running:
```bash
$ poetry run flask run
```

You should see output similar to the following:
```bash
 * Serving Flask app "app" (lazy loading)
 * Environment: development
 * Debug mode: on
 * Running on http://127.0.0.1:5000/ (Press CTRL+C to quit)
 * Restarting with fsevents reloader
 * Debugger is active!
 * Debugger PIN: 226-556-590
```
Now visit [`http://localhost:5000/`](http://localhost:5000/) in your web browser to view the app.

## Running Tests

Tests can be run from the terminal by running `poetry run pytest`

If you want to skip running the end-to-end tests, you can do this by running `poetry run pytest tests`. Similarly, if you only want to run the end-to-end tests, you can do this by running `poetry run pytest tests_e2e`

You can also run tests individually within VSCode by following the instructions on [this page](https://code.visualstudio.com/docs/python/testing#_configure-tests)

## Provision VM via Ansible

***Following instructions should be run on control node only***

1. Create a file called 'vault.yaml'
2. Use the following as a template, but replace placeholders with actual values
```yaml
trello_api_key: <trello_api_key>
trello_api_token: <trello_api_token>
trello_board_id: <trello_board_id>
```
3. Create a file called vault-pass and add a line with what you would like the pasword for the vault to be
4. Execute the following to encrypt the vault.yaml file created during step 2
```bash
ansible-vault encrypt --vault-password-file vault-pass vault.yaml
```
5. Run the playbook with following command
```bash
ansible-playbook playbook.yaml -i inventory.ini --vault-password-file vault-pass
```
6. If you need to view the content of the vault, you can run the following to decrypt
```bash
ansible-vault decrypt --vault-password-file vault-pass vault.yaml
```

## Run in Docker

#### Development

1. Build the image with the following command
   ```bash
   docker build --target development --tag todo-app:dev .
   ```
2. The container can be run via 2 ways
    1. Using a docker command 
       ```bash
       docker run --env-file .env --publish 5000:80 --mount type=bind,source="$(pwd)"/todo_app,target=/app/todo_app/todo_app todo-app:dev
       ```
    2. Using Docker Compose, which will also start test runners (this includes unit, integration and end-to-end tests) that will continously rerun tests when a change is detected **Note that for docker compose to work you will require the test images to be created. This can either be done manually in a similar way to the previous step, or add '--build' to the below command**
       ```bash
       docker compose up
       ```

#### Production

1. Build the image with the following command
   ```bash
   docker build --target production --tag todo-app:prod .
   ```
2. Run the container
   ```bash
   docker run --env-file .env --publish 5000:80 todo-app:prod
   ```

#### Debug

If you wish to create a container, but not start the application itself to debug, the following command can be used. You can also connect the container to VSCode to debug through that

```bash
docker compose -f docker-compose-debug.yaml up
```

# Azure Web App Setup (Manual)

## General Setup

Create an app service plan

```bash
az appservice plan create --resource-group <resource group> -n <app_service_plan_name> --sku B1 --is-linux
```

Create a web app

```bash
az webapp create --resource-group <resource group> --plan <app_service_plan_name> --name <web_app_name> --deployment-container-image-name docker.io/dmakwan312/todo-app:latest
```

## Set Configuration

### Option 1: Create app settings based on the env.json file

```bash
az webapp config appsettings set -g <resource group> -n <web_app_name>  --settings @env.json
```

### Option 2: Set up key vault

1. Create a Azure Secrets Vault
2. Assign yourself as the as the Key Vault Administrator from the Access Control (IAM) tab
3. Give the web app created in previous steps access screen as a Key Vault Secrets User
4. In the Environment Variables tab in the web app itself, update the value to the following
```
@Microsoft.KeyVault(VaultName=<vault_name>;SecretName=<secret_name_from_the_vault>)
```

## Create Docker Image

Create an image for the production stage and push it to docker repo

```bash
docker build --target production --tag dmakwan312/todo-app:latest .
docker push dmakwan312/todo-app:latest
```

Use the below curl command template to query the webhook URL of the web app. this will pull the latest image from docker and restart the application. The webhook url can be found from the Deployment Center tab for the web app

```
curl -dH -X POST "<webhook_url>"
```

# Encryption at Rest

Encryption at rest is enabled by default for Azure Cosmos DB including backups as well as media attachments and there are no options to disabled it. It is iomplemented during various methods such as secure key storage systems, encrypted networks and cryptographics APIs.

# Terraform

User the below command to set up terraform
```
terraform init
```

To push any changes use the below with actual values for the variables
```
terraform apply -var 'prefix=todo-dm' -var 'secret_key=$SECRET_KEY' -var 'trello_api_key=$TRELLO_API_KEY' -var 'trello_api_token=$TRELLO_API_TOKEN' -var 'trello_board_id=$TRELLO_BOARD_ID'
```