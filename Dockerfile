FROM python:3.11.4-buster AS base

RUN apt-get update

ENV PATH="/root/.local/bin:$PATH"
ENV PORT=80
EXPOSE $PORT

RUN curl -sSL https://install.python-poetry.org | python3 -

WORKDIR /app/todo_app
COPY pyproject.toml poetry.lock /app/todo_app/

FROM base AS production

RUN poetry config virtualenvs.create false --local && poetry install --without dev
COPY todo_app todo_app

ENTRYPOINT poetry run gunicorn --bind 0.0.0.0:$PORT "todo_app.app:create_app()"

FROM base AS development

RUN poetry install
ENTRYPOINT [ "/bin/bash", "-c", "poetry run flask run --host=0.0.0.0 --port=$PORT" ]

FROM base AS debug

ENTRYPOINT [ "tail", "-f", "/dev/null" ]

FROM base AS test_local

# Install dependencies
RUN apt-get update && \
    apt-get install -y --no-install-recommends gnupg wget curl unzip && \
    rm -rf /var/lib/apt/lists/*

# Add Google Chrome repository
RUN wget -qO - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add - && \
    echo "deb http://dl.google.com/linux/chrome/deb/ stable main" > /etc/apt/sources.list.d/google-chrome.list

# Install Google Chrome
RUN apt-get update && \
    apt-get install -y google-chrome-stable && \
    rm -rf /var/lib/apt/lists/*

# Install ChromeDriver
RUN CHROME_VERSION=$(google-chrome --product-version) &&\
    wget -O /tmp/chromedriver.zip https://storage.googleapis.com/chrome-for-testing-public/$CHROME_VERSION/linux64/chromedriver-linux64.zip
RUN unzip /tmp/chromedriver.zip chromedriver-linux64/chromedriver -d /usr/bin/chromedriver

RUN poetry install
COPY pytest.ini /app/todo_app/
COPY tests tests
COPY tests_e2e tests_e2e

ENTRYPOINT [ "poetry", "run", "ptw", "--poll", "todo_app" ]

FROM test_local AS test

COPY todo_app /app/todo_app/todo_app

ENTRYPOINT [ "poetry", "run", "pytest" ]